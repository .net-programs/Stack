﻿using System;
using System.Linq;

namespace Stack
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Stack<int> stack = new Stack<int>(Enumerable.Range(1, 3));

            Console.WriteLine("Стек:");
            foreach (var item in stack)
                Console.WriteLine(item);

            int count = stack.Count;
            for (int i = 0; i < count; i++)
                stack.Pop();

            Console.ReadLine();
        }
    }
}
