﻿using System;
using System.Collections;
using System.Diagnostics;

namespace Stack
{
    /// <summary>
    /// Collection's proxy type. This class can't be inherited.
    /// </summary>
    internal sealed class CollectionDebugView
    {
        private ICollection collection;

        /// <summary>
        /// Constructs collection's proxy type instance from particular collection.
        /// </summary>
        /// <param name="collection">Collection.</param>
        public CollectionDebugView(ICollection collection)
        {
            this.collection = collection ?? throw new ArgumentNullException(nameof(collection));
        }

        /// <summary>
        /// Collection's elements.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public object[] Items
        {
            get
            {
                object[] items = new object[collection.Count];
                collection.CopyTo(items, 0);
                return items;
            }
        }
    }
}