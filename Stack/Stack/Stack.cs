﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Stack
{
    /// <summary>
    /// Stack.
    /// </summary>
    /// <typeparam name="T">Values type.</typeparam>
    [DebuggerTypeProxy(typeof(CollectionDebugView))]
    [DebuggerDisplay("Count = {Count}")]
    [Serializable]
    public class Stack<T> : ICollection, IReadOnlyCollection<T>
    {
        private const int DefaultCapacity = 4;

        private readonly object syncRoot = new object();

        private int version;

        private T[] source;

        private void ThrowIfEmpty()
        {
            if (Count == 0)
                throw new InvalidOperationException($"Свойство {nameof(Count)} было равно {Count}.");
        }

        private void ThrowIfNull<TObject>(TObject _object)
        {
            if (_object == null)
                throw new ArgumentNullException($"Параметр {nameof(_object)} был равен {_object}.");
        }

        private void ThrowIfInvalidArrayRange(Array array, int index)
        {
            ThrowIfNull(array);

            if (index < 0 || index >= array.Length)
                throw new IndexOutOfRangeException(nameof(index));

            if (Count > array.Length - index)
                throw new ArgumentException(nameof(index));
        }

        private void InternalCopyTo(Array array, int index)
        {
            Array.Copy(source, 0, array, index, Count);
            Array.Reverse(array);
        }

        /// <summary>
        /// Collection's synchronization object.
        /// </summary>
        object ICollection.SyncRoot => syncRoot;

        /// <summary>
        /// Determines whether collection is synchronized.
        /// </summary>
        bool ICollection.IsSynchronized => false;

        /// <summary>
        /// Collection's size.
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Constructs stack with particular size.
        /// </summary>
        /// <param name="capacity">Size.</param>
        public Stack(int capacity = DefaultCapacity)
        {
            source = new T[capacity];
        }

        /// <summary>
        /// Constructs stack from particular collection.
        /// </summary>
        /// <param name="sequence">Collection.</param>
        public Stack(IEnumerable<T> sequence)
        {
            ThrowIfNull(sequence);

            source = new T[0];
            if (sequence is ICollection<T> collection)
            {
                Array.Resize(ref source, collection.Count);
                collection.CopyTo(source, 0);
                Count = collection.Count;
            }
            else
                foreach (var item in sequence)
                    Push(item);
        }

        /// <summary>
        /// Constructs empty stack.
        /// </summary>
        public Stack()
        {
            source = new T[DefaultCapacity];
        }

        /// <summary>
        /// Returns collection's enumerator.
        /// </summary>
        /// <returns>Collection's enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Returns collection's enumerator.
        /// </summary>
        /// <returns>Collection's enumerator.</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Copies collection to particular array.
        /// </summary>
        /// <param name="array">Array.</param>
        /// <param name="index">Index from which copying starts.</param>
        void ICollection.CopyTo(Array array, int index)
        {
            ThrowIfInvalidArrayRange(array, index);

            if (array.Rank != 1)
                throw new ArgumentOutOfRangeException(nameof(array.Rank));

            InternalCopyTo(array, index);
        }

        /// <summary>
        /// Returns collection's enumerator.
        /// </summary>
        /// <returns>Collection's enumerator.</returns>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        /// <summary>
        /// Copies collection to particular array.
        /// </summary>
        /// <param name="array">Array.</param>
        /// <param name="index">Index from which copying starts.</param>
        public void CopyTo(T[] array, int index)
        {
            ThrowIfInvalidArrayRange(array, index);
            InternalCopyTo(array, index);
        }

        /// <summary>
        /// Pushes particular item into stack.
        /// </summary>
        /// <param name="item">Item.</param>
        public void Push(T item)
        {
            if (Count == source.Length)
                Array.Resize(ref source, source.Length == 0 ? DefaultCapacity : source.Length * 2);
            source[Count++] = item;
            version++;
        }

        /// <summary>
        /// Retrives topmost item from stack.
        /// </summary>
        /// <returns>Topmost stack item.</returns>
        public T Pop()
        {
            ThrowIfEmpty();
            T result = source[--Count];
            source[Count] = default;
            version++;
            return result;
        }

        /// <summary>
        /// Gets topmost stack's item.
        /// </summary>
        /// <returns>Topmost stack's item.</returns>
        public T Peek()
        {
            ThrowIfEmpty();
            return source[Count - 1];
        }

        /// <summary>
        /// Evaluates whether particular item is in stack.
        /// </summary>
        /// <param name="item">Item to find.</param>
        /// <returns>true, if item presents in stack.</returns>
        public bool Contains(T item)
        {
            int i = source.Length - 1;
            IEqualityComparer<T> equalityComparer = EqualityComparer<T>.Default;
            while ((i >= 0) && !equalityComparer.Equals(item, source[i]))
                i--;
            return i >= 0;
        }

        /// <summary>
        /// Trims stack to actual number of elements.
        /// </summary>
        public void TrimExcess()
        {
            T[] array = new T[Count];
            source.CopyTo(array, 0);
            source = array;
            version++;
        }

        /// <summary>
        /// Clears collection.
        /// </summary>
        public void Clear()
        {
            source = new T[source.Length];
            Count = 0;
            version++;
        }

        /// <summary>
        /// Enumerator.
        /// </summary>
        public struct Enumerator : IEnumerator<T>
        {
            private readonly Stack<T> stack;
            private int index;
            private readonly int version;

            public void ThrowWhenCollectionWasChanged()
            {
                if (version != stack.version)
                    throw new InvalidOperationException("Collection was changed");
            }

            /// <summary>
            /// Current element.
            /// </summary>
            public T Current => stack.source[index];

            /// <summary>
            /// Current element.
            /// </summary>
            object IEnumerator.Current
            {
                get
                {
                    ThrowWhenCollectionWasChanged();
                    return Current;
                }
            }

            /// <summary>
            /// Constructs stack enumerator from stack.
            /// </summary>
            /// <param name="stack">Stack.</param>
            public Enumerator(Stack<T> stack)
            {
                this.stack = stack;
                index = -1;
                version = stack.version;
            }

            /// <summary>
            /// Releases all resources those were depended on this object.
            /// </summary>
            public void Dispose()
            {
            }

            /// <summary>
            /// Moves to next stack's element.
            /// </summary>
            /// <returns>Value which indicates whether futher motion is able.</returns>
            public bool MoveNext()
            {
                ThrowWhenCollectionWasChanged();

                index++;
                return index < stack.Count;
            }

            /// <summary>
            /// Resets enumerator to position which is befor first stack element.
            /// </summary>
            public void Reset()
            {
                ThrowWhenCollectionWasChanged();
                index = -1;
            }
        }
    }
}
